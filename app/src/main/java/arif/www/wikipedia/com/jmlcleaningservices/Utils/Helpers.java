package arif.www.wikipedia.com.jmlcleaningservices.Utils;

import android.content.Context;
import android.net.ConnectivityManager;

public class Helpers {

    public static Boolean checkConnection(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm != null && cm.getActiveNetworkInfo() != null;
    }
}
