package arif.www.wikipedia.com.jmlcleaningservices.Models

import android.widget.TextView

class Gallery(
        val id: Int,
        val name: String,
        val galleryType: Int,
        val type: GalleryType,
        val contextId: Int,
        val context: String,
        val description: String,
        val source: String,
        val active: Boolean,
        val added_time: String,
        val images_count: Int,
        val images: List<GalleryImage>
) {

    override fun toString(): String {
        return name
    }
}