package arif.www.wikipedia.com.jmlcleaningservices.Models

class User(
        val id: Int,
        val first_name: String,
        val last_name: String,
        val user_name: String,
        val password: String,
        val email: String,
        val image: String
) {

}