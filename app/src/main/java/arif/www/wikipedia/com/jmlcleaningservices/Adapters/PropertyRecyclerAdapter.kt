package arif.www.wikipedia.com.jmlcleaningservices.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import arif.www.wikipedia.com.jmlcleaningservices.Models.Property
import arif.www.wikipedia.com.jmlcleaningservices.PropertyDetailActivity
import arif.www.wikipedia.com.jmlcleaningservices.PropertyDetailNewActivity
import arif.www.wikipedia.com.jmlcleaningservices.R
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP
import kotlinx.android.synthetic.main.property_list_item.view.*

class PropertyRecyclerAdapter(val context: Context, var properties: List<Property>) : RecyclerView.Adapter<PropertyRecyclerAdapter.ViewHolder>()  {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PropertyRecyclerAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.property_list_item, parent, false);
        return ViewHolder(view)
    }

    override fun getItemCount() = properties.size

    override fun onBindViewHolder(holder: PropertyRecyclerAdapter.ViewHolder, position: Int) {
        val property = properties[position]
        holder.setData(property);
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        //2
        private var view: View = v
        private var property: Property? = null

        fun setData(property: Property?) {
            this.property = property;
            view.clientName.text = property?.client?.name;
            view.address.text = "${property!!.address}, ${property!!.address_suburb}, ${property!!.address_state}, ${property!!.address_post_code}";
        }

        //3
        init {
            v.setOnClickListener(this)
        }

        //4
        override fun onClick(v: View) {
            val intent = Intent(context, PropertyDetailNewActivity::class.java);
            SP.setInt(context, SP.CURRENT_PROPERTY_CLIENT_ID, property!!.client.id)
            println("Property ID: " +  property!!.id);
            println("Property Client ID: " +  property!!.client_id + " <-> " + property!!.client.id);
            println("Property Client Child of: " +  property!!.client.child_of);

            SP.setString(context, SP.CURRENT_PROPERTY_CLIENT_NAME, property!!.client.name)
            SP.setString(context, SP.CURRENT_PROPERTY_LOCATION, property!!.address_location)
            SP.setString(context, SP.CURRENT_PROPERTY_ADDRESS, "${property!!.address}, ${property!!.address_suburb}, ${property!!.address_state}, ${property!!.address_post_code}")
            SP.setInt(context, SP.CURRENT_PROPERTY_ID, property!!.id)
            SP.setBoolean(context, SP.CURRENT_PROPERTY_IS_PARENT, property!!.client.is_parent)
            SP.setInt(context, SP.CURRENT_PROPERTY_IS_CHILD_OF, property!!.client.child_of)
            SP.setString(context, SP.CURRENT_PROPERTY_NOTES, property!!.notes)
            context.startActivity(intent);
        }
    }

}