package arif.www.wikipedia.com.jmlcleaningservices.NetworkModels

import arif.www.wikipedia.com.jmlcleaningservices.Models.Gallery
import arif.www.wikipedia.com.jmlcleaningservices.Models.Property
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import java.util.*

data class GallerySingleProvider(val id: Int, val type: String, val description: String, val active: Boolean) {

    class Deserializer : ResponseDeserializable<Gallery>{
        override fun deserialize(content: String): Gallery? = Gson().fromJson(content, Gallery::class.java);
    }

}
