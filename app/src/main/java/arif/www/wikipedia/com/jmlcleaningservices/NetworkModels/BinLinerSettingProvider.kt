package arif.www.wikipedia.com.jmlcleaningservices.NetworkModels

import arif.www.wikipedia.com.jmlcleaningservices.Models.Property
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class BinLinerSettingProvider(val id: Int, val name: String, val price: Double, val active: Boolean) {
    class Deserializer : ResponseDeserializable<Array<BinLinerSettingProvider>>{
        override fun deserialize(content: String): Array<BinLinerSettingProvider>? = Gson().fromJson(content, Array<BinLinerSettingProvider>::class.java);
    }
}
