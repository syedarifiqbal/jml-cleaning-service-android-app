package arif.www.wikipedia.com.jmlcleaningservices.Utils

import android.content.Context
import android.preference.PreferenceManager

class Settings(val context: Context){

    companion object {
        fun getBool(context: Context, key: String) : Boolean {
            val settings = PreferenceManager.getDefaultSharedPreferences(context)
            return settings.getBoolean(key, false)
        }
    }

}