package arif.www.wikipedia.com.jmlcleaningservices.Adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import arif.www.wikipedia.com.jmlcleaningservices.*
import arif.www.wikipedia.com.jmlcleaningservices.Models.PropertySingleItem
import kotlinx.android.synthetic.main.property_list_item.view.*
import kotlinx.android.synthetic.main.property_single_list_item.view.*

class PropertySingleRecyclerAdapter(val context: Context, var properties: List<PropertySingleItem>) : RecyclerView.Adapter<PropertySingleRecyclerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PropertySingleRecyclerAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.property_single_list_item, parent, false);
        return ViewHolder(view)
    }

    override fun getItemCount() = properties.size

    override fun onBindViewHolder(holder: PropertySingleRecyclerAdapter.ViewHolder, position: Int) {
        val property = properties[position]
        holder.setData(property);
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        //2
        private var view: View = v
        private var item: PropertySingleItem? = null

        fun setData(item: PropertySingleItem?) {
            this.item = item;
            view.title.text = item?.title;
            view.icon.setImageResource(item!!.image);
            view.icon.setOnClickListener {
                v ->
                if (item?.title == "Gallery") {
                    val intent = Intent(context, PropertyGalleryActivity::class.java);
                    val activity = context as Activity;
                    activity.startActivity(intent);
                }
                if (item?.title == "Property Info") {
                    val intent = Intent(context, PropertyInfoActivity::class.java);
                    val activity = context as Activity;
                    activity.startActivity(intent);
                }
                if (item?.title == "Instructions") {
                    val intent = Intent(context, PropertyInstructionActivity::class.java);
                    val activity = context as Activity;
                    activity.startActivity(intent);
                }
                if (item?.title == "Security") {
                    val intent = Intent(context, PropertySecurityActivity::class.java);
                    val activity = context as Activity;
                    activity.startActivity(intent);
                }
                if (item?.title == "Bin liner") {
                    val intent = Intent(context, PropertyBinLinerActivity::class.java);
                    val activity = context as Activity;
                    activity.startActivity(intent);
                }
            }
        }

        //3
        init {
            v.setOnClickListener(this)
        }

        //4
        override fun onClick(v: View) {
            /*if (item?.title == "Gallery") {
                val intent = Intent(context, PropertyGalleryActivity::class.java);
                val activity = context as Activity;
                activity.startActivity(intent);
            }
            if (item?.title == "Issues") {
                val intent = Intent(context, GallerySliderJavaActivity::class.java);
                val activity = context as Activity;
                activity.startActivity(intent);
            }*/
        }
    }

}