package arif.www.wikipedia.com.jmlcleaningservices.Models

class GalleryImage(
        val id: Int,
        val url: String,
        val image: String
) {

}