package arif.www.wikipedia.com.jmlcleaningservices

import android.support.v7.app.AppCompatActivity

import android.support.v4.app.FragmentPagerAdapter
import android.os.Bundle
import arif.www.wikipedia.com.jmlcleaningservices.Adapters.GalleryPagerAdapter
import arif.www.wikipedia.com.jmlcleaningservices.Models.GalleryImage
import arif.www.wikipedia.com.jmlcleaningservices.NetworkModels.GallerySingleProvider
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP
import com.github.kittinunf.fuel.httpGet

import kotlinx.android.synthetic.main.activity_gallery_slider.*

class GallerySliderActivity : AppCompatActivity() {

    var galleryList: ArrayList<GalleryImage>? = null;

    private var pagerAdapter: GalleryPagerAdapter? = null
    private var GALLERY_ID = 0;
    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: GalleryPagerAdapter? = null
//    var pagerAdapter: GalleryPagerAdapter;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery_slider)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true);

        toolbar.setTitle(SP.getString(this, SP.CURRENT_PROPERTY_ADDRESS, ""))
        GALLERY_ID = intent.getIntExtra("GALLERY_ID", 0)
        val images = ArrayList<GalleryImage>()

        "/galleries/${GALLERY_ID}?token=${SP.getString(this, SP.TOKEN, "")}"
                .httpGet().responseObject(GallerySingleProvider.Deserializer()) { _, response, result ->
                    val (gallerySingleProvider, error) = result;
                    //*println(PropertyProvider.toString());
                    /*println(response.statusCode);
                    println(response.url);
                    println(error);*/
                    galleryList = gallerySingleProvider?.images as ArrayList<GalleryImage>;
                    pagerAdapter = GalleryPagerAdapter(this, galleryList)
                    container.adapter = pagerAdapter
                }

        /*fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }*/

    }

    /*override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_gallery_slider, menu)
        return true
    }*/

    /*override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }*/


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    /*inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1)
        }
        override fun getCount(): Int {
            // Show 3 total pages.
            return galleryList!!.size
        }
    }

    *//**
     * A placeholder fragment containing a simple view.
     *//*
    class PlaceholderFragment : Fragment() {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            val rootView = inflater.inflate(R.layout.fragment_gallery_slider, container, false)
//            rootView.section_label.text = getString(R.string.section_format, arguments?.getInt(ARG_SECTION_NUMBER))
            return rootView
        }

        companion object {
            *//**
             * The fragment argument representing the section number for this
             * fragment.
             *//*
            private val ARG_SECTION_NUMBER = "section_number"

            *//**
             * Returns a new instance of this fragment for the given section
             * number.
             *//*
            fun newInstance(sectionNumber: Int): PlaceholderFragment {
                val fragment = PlaceholderFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                return fragment
            }
        }
    }*/
}
