package arif.www.wikipedia.com.jmlcleaningservices.Models

class Job(
    val id: Int,
    val client_id: Int,
    val property_id: Int,
    val job_category: Int,
    val job_type: Int,
    val job_title: String,
    val instruction: String,
    val start_date: String,
    val end_date: String,
    val duration: Int,
    val duration_schedule: String,
    val start_time: String,
    val end_time: String,
    val visit_frequency: String,
    val notes: String,
    val internal_notes: String,
    val next_visit: String,
    val added_time: String,
    val added_by: Int,
    val frequency: String,
    val every_no_day: Int,
    val week_days: String,
    val month_type: String,
    val month_day_or_week: String,
    val selected_day_of_month: String,
    val closed: Int,
    val owner: Client,
    val property: Property,
    val visits: ArrayList<Visit>
){}


class Visit(
    val id: Int,
    val title: String,
    val description: String,
    val date: String,
    val job_id: Int,
    val completed: Int,
    val notes: String,
    val time_in: String,
    val time_out: String,
    val user_id: Int,
    val logged_at: String
){}