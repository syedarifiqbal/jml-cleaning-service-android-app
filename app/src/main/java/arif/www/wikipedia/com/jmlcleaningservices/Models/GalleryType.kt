package arif.www.wikipedia.com.jmlcleaningservices.Models

class GalleryType(
        val id: Int,
        val type: String,
        val description: String
) {

    override fun toString(): String {
        return type
    }
}