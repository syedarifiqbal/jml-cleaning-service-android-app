package arif.www.wikipedia.com.jmlcleaningservices.Models

class Contact(
        val id: Int,
        val contact_name: String,
        val surname: String,
        val role: String,
        val phone: String,
        val email: String,
        val is_primary: Boolean,
        val client_id: Int,
        val active: Boolean
) {

}