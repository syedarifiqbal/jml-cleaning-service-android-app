package arif.www.wikipedia.com.jmlcleaningservices.Adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import arif.www.wikipedia.com.jmlcleaningservices.GallerySliderActivity
import arif.www.wikipedia.com.jmlcleaningservices.Models.Gallery
import arif.www.wikipedia.com.jmlcleaningservices.R
import kotlinx.android.synthetic.main.gallery_list_item.view.*
import kotlinx.android.synthetic.main.property_list_item.view.*

class GalleryRecyclerAdapter(val context: Context, var galleries: List<Gallery>) : RecyclerView.Adapter<GalleryRecyclerAdapter.ViewHolder>()  {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryRecyclerAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.gallery_list_item, parent, false);
        return ViewHolder(view)
    }

    override fun getItemCount() = galleries.size

    override fun onBindViewHolder(holder: GalleryRecyclerAdapter.ViewHolder, position: Int) {
        val gallery = galleries[position]
        holder.setData(gallery);
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        //2
        private var view: View = v
        private var gallery: Gallery? = null

        fun setData(gallery: Gallery?) {
            this.gallery = gallery;
            view.galleryName.text = gallery?.name;
            view.description.text = gallery?.description;
            view.createdAt.text = gallery?.added_time;
            view.galleryType.text = gallery?.type?.type;
            view.count_images.text = "Images: ${gallery?.images_count}";
        }

        //3
        init {
            v.setOnClickListener(this)
        }

        //4
        override fun onClick(v: View) {
            val intent = Intent(context, GallerySliderActivity::class.java);
            intent.putExtra("GALLERY_ID", gallery?.id)
            context.startActivity(intent);
        }
    }

}