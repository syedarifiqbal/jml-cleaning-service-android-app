package arif.www.wikipedia.com.jmlcleaningservices

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.view.View
import android.widget.Toast
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.activity_property_key_image.*

class PropertyKeyImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_key_image)
        setSupportActionBar(toolbar)

        supportActionBar?.setTitle(intent.getStringExtra("title"))

        Picasso.get()
                .load(intent.getStringExtra("src"))
                .fit()
                .centerInside()
                .into(myImageView, object : Callback {
                    override fun onSuccess() {
                        keyPreloader.setVisibility(View.GONE)
                    }
                    override fun onError(e: Exception) {
                        Toast.makeText(applicationContext, e.message, Toast.LENGTH_LONG).show()
                    }
                });

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

}
