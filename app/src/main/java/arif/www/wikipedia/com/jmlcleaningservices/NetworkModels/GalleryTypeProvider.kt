package arif.www.wikipedia.com.jmlcleaningservices.NetworkModels

import arif.www.wikipedia.com.jmlcleaningservices.Models.Property
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class GalleryTypeProvider(val id: Int, val type: String, val description: String, val active: Boolean) {

    class Deserializer : ResponseDeserializable<Array<GalleryTypeProvider>>{
        override fun deserialize(content: String): Array<GalleryTypeProvider>? = Gson().fromJson(content, Array<GalleryTypeProvider>::class.java);
    }
}
