package arif.www.wikipedia.com.jmlcleaningservices.RetrofitServices;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface GalleryTypeService {
    @Multipart
    @POST("galleries")
    Call<ResponseBody> upload(
            @Query("token") String token,
            @Part("description") RequestBody description,
            @Part("gallery_type_id") RequestBody gallery_type_id,
            @Part("property_id") RequestBody property_id,
            @Part("type") RequestBody type,
            @Part("size") RequestBody size,
            @Part List<MultipartBody.Part> files,
            @Part("isPortraits") RequestBody isFilesPortrait);

    @GET("/gallery/types")
    Call<JsonArray> get(@Query("token") String token);
}
