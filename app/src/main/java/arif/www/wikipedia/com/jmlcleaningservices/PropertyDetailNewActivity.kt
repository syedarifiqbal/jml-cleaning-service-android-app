package arif.www.wikipedia.com.jmlcleaningservices

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import android.widget.Toast
import arif.www.wikipedia.com.jmlcleaningservices.Adapters.PropertySingleRecyclerAdapter
import arif.www.wikipedia.com.jmlcleaningservices.Models.PropertySingleItem
import arif.www.wikipedia.com.jmlcleaningservices.NetworkModels.ClientProvider
import arif.www.wikipedia.com.jmlcleaningservices.NetworkModels.PropertyProvider
import arif.www.wikipedia.com.jmlcleaningservices.NetworkModels.PropertySingleProvider
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP
import com.github.kittinunf.fuel.httpGet
import com.google.gson.Gson
import com.mapbox.mapboxsdk.maps.MapView
import kotlinx.android.synthetic.main.activity_properties.*
import kotlinx.android.synthetic.main.content_property_detail_new.*

import java.util.ArrayList

class PropertyDetailNewActivity : AppCompatActivity() {
    private var mapView: MapView? = null
    private var adapter: PropertySingleRecyclerAdapter? = null
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var recyclerView: RecyclerView

    //    companion object {
    var propertyId = 0
    var clientName = ""
    var address = ""
    val REQUEST_CODE = 1001
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_detail_new)
        setSupportActionBar(findViewById(R.id.toolbar));
        propertyId = SP.getInt(this, SP.CURRENT_PROPERTY_ID, 0);
        clientName = SP.getString(this, SP.CURRENT_PROPERTY_CLIENT_NAME,"");
        address = SP.getString(this, SP.CURRENT_PROPERTY_ADDRESS, "");
//        toolbar_layout.setTitle(clientName);
        recyclerView = findViewById(R.id.ac_property_recycler);

        val clientNameTv = findViewById<TextView>(R.id.clientName)
        val addressTv = findViewById<TextView>(R.id.address)
        clientNameTv.text = clientName
        addressTv.text = address

        setParentProperty()

        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()*/

            /*val intent = Intent(this, PropertyMapActivity::class.java)
            startActivity(intent);*/
            // Create a Uri from an intent string. Use the result to create an Intent.
            val location = SP.getString(this, SP.CURRENT_PROPERTY_LOCATION, "");
            val locationObj = Gson().fromJson(location, PropertyDetailActivity.LocationModel::class.java)
            /*val gmmIntentUri = Uri.parse("google.streetview:cbll=${locationObj.lat},${locationObj.lng}")
            Toast.makeText(applicationContext, location, Toast.LENGTH_LONG).show()
            // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            // Make the Intent explicit by setting the Google Maps package
            mapIntent.setPackage("com.google.android.apps.maps")

            // Attempt to start an activity that can handle the Intent
             startActivity(mapIntent)*/

            // 53 Pakenham Street, Fremantle WA 6160, Australia
            // val gmmIntentUri = Uri.parse("geo:${locationObj.lat},${locationObj.lng}(Google+Sydney)")
            val address = SP.getString(this, SP.CURRENT_PROPERTY_ADDRESS,"")
            val gmmIntentUri = Uri.parse("google.navigation:q=${address.replace(",", " ")}")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            Toast.makeText(applicationContext, address.replace(",", " "), Toast.LENGTH_LONG).show()
            if (mapIntent.resolveActivity(packageManager) != null) {
                startActivity(mapIntent)
            }
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupOptionsIcon()
    }

    private fun setParentProperty(){

        if(SP.getBoolean(this, SP.CURRENT_PROPERTY_IS_PARENT, false)){ return; }
        "/clients/${SP.getInt(this, SP.CURRENT_PROPERTY_IS_CHILD_OF, 0)}?token=${SP.getString(this, SP.TOKEN, "")}"
                .httpGet().responseObject(ClientProvider.Deserializer()) { _, response, result ->
                    val (client, error) = result;
                    parentName.text = client?.name
                    parentAddress.text = "${client?.address_1}, ${client?.address_suburb}, ${client?.address_state}, ${client?.address_post_code}"
                    infoCardParent.visibility = View.VISIBLE
                }
    }

    fun showProgressbar(isVisitble: Boolean) {
        /*if (isVisitble) {
            progressBar.setVisibility(View.VISIBLE);
            clientRecyclerView.visibility = View.GONE;
            return;
        }
        progressBar.setVisibility(View.GONE);
        clientRecyclerView.visibility = View.VISIBLE;*/
    }

    // this function will add 6 icons for this particular property's related things
    private fun setupOptionsIcon() {
        gridLayoutManager = GridLayoutManager(this, 2)
        recyclerView.layoutManager = gridLayoutManager

        val items = ArrayList<PropertySingleItem>();
        items.add(PropertySingleItem("Issues", R.mipmap.issues));
        items.add(PropertySingleItem("Gallery", R.mipmap.gallery));
        items.add(PropertySingleItem("Property Info", R.mipmap.property_info));
        items.add(PropertySingleItem("Security", R.mipmap.security));
        items.add(PropertySingleItem("Supplies", R.mipmap.supplies));
        items.add(PropertySingleItem("Instructions", R.mipmap.instructions));
        items.add(PropertySingleItem("Bin liner", R.mipmap.bin_liners));
        adapter = PropertySingleRecyclerAdapter(this, items);
        recyclerView.adapter = adapter;
    }

}
