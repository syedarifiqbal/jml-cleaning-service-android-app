package arif.www.wikipedia.com.jmlcleaningservices

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.TextView
import android.widget.Toast
import arif.www.wikipedia.com.jmlcleaningservices.Adapters.JobRecyclerAdapter
import arif.www.wikipedia.com.jmlcleaningservices.Adapters.PropertyRecyclerAdapter
import arif.www.wikipedia.com.jmlcleaningservices.Models.Job
import arif.www.wikipedia.com.jmlcleaningservices.NetworkModels.JobProvider
import arif.www.wikipedia.com.jmlcleaningservices.NetworkModels.PropertyProvider
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.httpGet
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_properties.*

import kotlinx.android.synthetic.main.activity_schedule.*
import kotlinx.android.synthetic.main.content_schedule.*
import java.net.URL
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.properties.Delegates
import android.support.v4.app.SupportActivity
import android.support.v4.app.SupportActivity.ExtraData
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import kotlin.collections.HashMap


class ScheduleActivity : AppCompatActivity() {

    lateinit var calendar: Calendar;
    lateinit var selectedCalendar: Calendar;
    var selectedDay: Int by Delegates.notNull<Int>();
    var selectedMonth: Int by Delegates.notNull<Int>();
    var selectedYear: Int by Delegates.notNull<Int>();
    var jobList = ArrayList<Job>()
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: JobRecyclerAdapter
    private var selectedDateList = HashMap<String, Calendar>();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule)
        setSupportActionBar(toolbar)

        FuelManager.instance.basePath = "http://api.jmlcleaningservices.com.au"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        monthTitle.text = getMonth().toUpperCase();

        linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL

        jobRecyclerView.layoutManager = linearLayoutManager
        adapter = JobRecyclerAdapter(this, jobList);
        jobRecyclerView.adapter = adapter;

        initCalendar()

        // displayUserImage("https://ui-avatars.com/api/?name=User+Name");

        loadJobs()

    }

    private fun loadJobs() {
        displayProgress(true)
        val dateQuery = SimpleDateFormat("yyyy-MM-dd").format(selectedCalendar.time);

        "/jobs?token=${SP.getString(this, SP.TOKEN, "")}&date=${dateQuery}"
                .httpGet().responseObject(JobProvider.Deserializer()) { _, response, result ->
                    val (JobProvider, error) = result;

                    jobList = ArrayList<Job>();

                    JobProvider?.data?.forEach { jobList.add(it); }

                    adapter.jobs = jobList

                    adapter.notifyDataSetChanged()
                    jobRecyclerView.adapter = adapter

                    displayProgress(false)
                }
    }

    private fun displayUserImage(url: String) {
        Picasso.get()
                .load(url)
                .resize(64, 64)
                .fit()
                .centerInside()
                .into(userImage, object : Callback {
                    override fun onSuccess() {
                    }
                    override fun onError(e: Exception) {
                        // Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
                    }
                })
    }

    private fun initCalendar()
    {
        calendar = Calendar.getInstance()
        selectedYear = calendar.get(Calendar.YEAR)
        selectedMonth = calendar.get(Calendar.MONTH)
        selectedDay = calendar.get(Calendar.DAY_OF_MONTH)

        setSelectedCalendar();

        setDates()
    }

    private fun setSelectedCalendar() {
        selectedCalendar = Calendar.getInstance();
        selectedCalendar.set(Calendar.YEAR, selectedYear);
        selectedCalendar.set(Calendar.MONTH, selectedMonth);
        selectedCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);
    }

    @SuppressLint("SimpleDateFormat", "DefaultLocale")
    fun onDatepickerOpenHandler(view: View){

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            calendar.set(year, monthOfYear, dayOfMonth);

            val sm = SimpleDateFormat("MMMM").format(calendar.time);
            val day = SimpleDateFormat("E").format(calendar.time);
            // Display Selected date in textbox
            monthTitle.text = sm.toUpperCase();

            selectedYear = year
            selectedMonth = monthOfYear
            selectedDay = dayOfMonth
            setSelectedCalendar();

            setDates()
            loadJobs()

        }, selectedYear, selectedMonth, selectedDay)

        dpd.show()
    }

    private fun setDates()
    {
        val backCalendar = Calendar.getInstance();
        backCalendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))

        for (i in 1..7) {
            val day = calendar.get(Calendar.DAY_OF_WEEK)
            val date = calendar.get(Calendar.DAY_OF_MONTH)

            if(date == selectedCalendar.get(Calendar.DAY_OF_MONTH))
            {
                getHeader(day).setTextColor(Color.parseColor("#ffffff"))
                getValue(day).setTextColor(Color.parseColor("#ffffff"))
            }else{
                getHeader(day).setTextColor(Color.parseColor("#DADEDF"))
                getValue(day).setTextColor(Color.parseColor("#DADEDF"))
            }

            if(day >= i)
            {
                val calx = Calendar.getInstance();
                calx.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                selectedDateList.put(day.toString(), calx);

                getValue(day).text = "${date}"
                calendar.add(Calendar.DATE, 1);
            }
        }

        val day = backCalendar.get(Calendar.DAY_OF_WEEK)

        var date = backCalendar.get(Calendar.DAY_OF_MONTH)

        backCalendar.add(Calendar.DATE, -day);

        for (i in 1..day)
        {
            backCalendar.add(Calendar.DATE, 1);
            if(i < day)
            {
                val calx = Calendar.getInstance();
                calx.set(backCalendar.get(Calendar.YEAR), backCalendar.get(Calendar.MONTH), backCalendar.get(Calendar.DAY_OF_MONTH));
                selectedDateList.put(day.toString(), calx);

                date = backCalendar.get(Calendar.DAY_OF_MONTH)
                getValue(i).text = "${date}"
            }
        }
    }

    private fun getValue(day:Int): TextView {
        return when(day)
        {
            1 -> valueSun
            2 -> valueMon
            3 -> valueTue
            4 -> valueWed
            5 -> valueThu
            6 -> valueFri
            else -> valueSat
        }
    }

    private fun getHeader(day:Int): TextView {
        return when(day)
        {
            1 -> headingSun
            2 -> headingMon
            3 -> headingTue
            4 -> headingWed
            5 -> headingThu
            6 -> headingFri
            else -> headingSat
        }
    }

    fun onDateCellClicked(view: View){

        val weekday = findViewById<TextView>(view.id).contentDescription;

        val c = selectedDateList.getValue(weekday.toString());

        selectedYear = c.get(Calendar.YEAR)
        selectedMonth = c.get(Calendar.MONTH)
        selectedDay = c.get(Calendar.DAY_OF_MONTH)
        calendar.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH))
        setSelectedCalendar();

        // println(SimpleDateFormat("E MMM YYYY").format(selectedDateList.getValue(weekday.toString()).time));
        // selectedDay = findViewById<TextView>(view.id).text.toString().toInt()
        setDates()
        loadJobs()
        // Toast.makeText(this, findViewById<TextView>(view.id).contentDescription, Toast.LENGTH_LONG).show();
    }

    @SuppressLint("SimpleDateFormat")
    @Throws(ParseException::class)
    private fun getMonth(): String {
        val cal = Calendar.getInstance()
        return SimpleDateFormat("MMMM").format(cal.time)
    }

    fun displayProgress(show: Boolean)
    {
        if(show)
        {
            userInfoContainer.visibility = View.GONE
            jobRecyclerView.visibility = View.GONE
            loadingbar.visibility = View.VISIBLE
        }else {
            userInfoContainer.visibility = View.VISIBLE
            jobRecyclerView.visibility = View.VISIBLE
            loadingbar.visibility = View.GONE
        }
    }

}