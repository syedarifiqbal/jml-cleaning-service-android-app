package arif.www.wikipedia.com.jmlcleaningservices

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP

import kotlinx.android.synthetic.main.activity_property_instruction.*
import kotlinx.android.synthetic.main.content_property_instruction.*

class PropertyInstructionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_instruction)
        setSupportActionBar(toolbar)

        val notesText= SP.getString(this, SP.CURRENT_PROPERTY_NOTES,"")

        notes.text = if(notesText.length>0) notesText else "Notes is not available";

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

}
