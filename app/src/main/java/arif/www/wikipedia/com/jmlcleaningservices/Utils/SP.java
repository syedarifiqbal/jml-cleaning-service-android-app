package arif.www.wikipedia.com.jmlcleaningservices.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

import org.jetbrains.annotations.Nullable;

public class SP {

    private final static String PREF_FILE = "JML_CLEANING";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String DP_IMAGE_URL = "dp_image_url";
    public static final String FULL_NAME = "full_name";
    public static final String EMAIL = "email";
    public static final String USER_ID = "id";
    public static final String USERNAME = "username";
    public static final String TOKEN = "token";
    public static final String CURRENT_PROPERTY_ID = "current_property_id";
    public static final String CURRENT_PROPERTY_LOCATION = "current_property_location";
    public static final String CURRENT_PROPERTY_CLIENT_NAME = "current_property_client_name";
    public static final String CURRENT_PROPERTY_ADDRESS = "current_property_address";
    public static final String CURRENT_PROPERTY_NOTES = "current_property_notes";
    public static final String CURRENT_PROPERTY_IS_PARENT = "is_parent";
    public static final String CURRENT_PROPERTY_IS_CHILD_OF = "child_of";
    public static final String CURRENT_PROPERTY_CLIENT_ID = "current_property_client_id";

    /**
     * Set a string shared preference
     * @param key - Key to set shared preference
     * @param value - Value for the key
     */
    public static void setString(Context context, String key, String value){
        SharedPreferences settings = context.getSharedPreferences(PREF_FILE,0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Set a integer shared preference
     * @param key - Key to set shared preference
     * @param value - Value for the key
     */
    public static void setInt(Context context, String key, int value){
        SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * Set a Boolean shared preference
     * @param key - Key to set shared preference
     * @param value - Value for the key
     */
    public static void setBoolean(Context context, String key, boolean value){
        SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     * Get a string shared preference
     * @param key - Key to look up in shared preferences.
     * @param defValue - Default value to be returned if shared preference isn't found.
     * @return value - String containing value of the shared preference if found.
     */
    public static String getString(Context context, String key, String defValue){

        SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
        return settings.getString(key, defValue);
    }

    /**
     * Get a integer shared preference
     * @param key - Key to look up in shared preferences.
     * @param defValue - Default value to be returned if shared preference isn't found.
     * @return value - String containing value of the shared preference if found.
     */
    public static int getInt(Context context, String key, int defValue){
        SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
        return settings.getInt(key, defValue);
    }

    /**
     * Get a boolean shared preference
     * @param key - Key to look up in shared preferences.
     * @param defValue - Default value to be returned if shared preference isn't found.
     * @return value - String containing value of the shared preference if found.
     */
    public static boolean getBoolean(Context context, String key, boolean defValue){
        SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
        return settings.getBoolean(key, defValue);
    }
}
