package arif.www.wikipedia.com.jmlcleaningservices

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import arif.www.wikipedia.com.jmlcleaningservices.NetworkModels.PropertySingleProvider
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP
import com.github.kittinunf.fuel.httpGet

import kotlinx.android.synthetic.main.activity_property_security.*
import kotlinx.android.synthetic.main.content_property_security.*
import kotlinx.android.synthetic.main.property_key_row.*

class PropertySecurityActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_security)
        setSupportActionBar(toolbar)

        "/properties/${SP.getInt(this, SP.CURRENT_PROPERTY_ID, 0)}/keys?token=${SP.getString(this, SP.TOKEN, "")}"
                .httpGet().responseObject(PropertySingleProvider.Deserializer()) { _, response, result ->
                    val (property, err) = result
                    if(property?.keys != null && property.keys.isNotEmpty())
                    {
                        var i = 0
                        property.keys.forEach{
                            val li = applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater;
                            val view = li.inflate(R.layout.property_key_row, null);

                            val type = view.findViewById<TextView>(R.id.name);
                            type.text = it.type

                            val description = view.findViewById<TextView>(R.id.description);
                            description.text = it.pivot.description

                            val keyNo= view.findViewById<TextView>(R.id.keyNo);
                            keyNo.text = "KEY-${it.pivot.id}"

                            val internalReference = view.findViewById<TextView>(R.id.internalReference);
                            internalReference.text = "Internal Reference: ${it.pivot.internal_reference}"

                            val viewImage = view.findViewById<TextView>(R.id.viewImage);
                            if(it.pivot.image.isEmpty())
                            {
                                viewImage.visibility = View.GONE
                            }else{
                                viewImage.setOnClickListener() { v ->
                                    val intent = Intent(this, PropertyKeyImageActivity::class.java)
                                    intent.putExtra("title", "Internal Reference ${it.pivot.internal_reference}")
                                    intent.putExtra("src", "http://www.jmlcleaningservices.com.au/jml/uploads/property_keys/${it.pivot.image}");
                                    startActivity(intent);
                                }
                            }
                            keyContainer.addView(view, i);
                            i++
                        }
                    }
                    else
                    {
                        notFoundMsg.visibility = View.VISIBLE
                    }
                    progressBar2.visibility = View.GONE
                }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

}
