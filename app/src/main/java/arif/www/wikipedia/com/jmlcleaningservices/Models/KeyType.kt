package arif.www.wikipedia.com.jmlcleaningservices.Models

class KeyType(
        val id: Int,
        val type: String,
        val description: String,
        val image: String,
        val active: Boolean,
        val pivot: KeyPivot
) {

}

class KeyPivot( val id: Int,
                val property_id: Int,
                val key_type_id: Int,
                val description: String,
                val internal_reference: String,
                val image: String
){
}