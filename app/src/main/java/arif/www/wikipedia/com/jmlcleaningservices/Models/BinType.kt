package arif.www.wikipedia.com.jmlcleaningservices.Models

class BinType(
        val id: Int,
        val type: String,
        val size: Int,
        val color: String,
        val description: String,
        val image: String,
        val active: Boolean,
        val pivot: BinPivot
) {

}

class BinPivot ( val property_id: Int, val bin_type: Int, val qty: Int ){}