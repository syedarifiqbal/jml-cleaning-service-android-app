package arif.www.wikipedia.com.jmlcleaningservices

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.*
import arif.www.wikipedia.com.jmlcleaningservices.Models.GalleryType
import arif.www.wikipedia.com.jmlcleaningservices.NetworkModels.GalleryTypeProvider
import arif.www.wikipedia.com.jmlcleaningservices.RetrofitServices.GalleryTypeService
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.DataPart
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Headers
import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.httpGet
import com.mapbox.mapboxsdk.utils.FileUtils
import kotlinx.android.synthetic.main.activity_gallery_request.*
import okhttp3.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.io.File

class GalleryRequestActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private var galleryTypeLists:ArrayList<GalleryType> = ArrayList<GalleryType>();
    private lateinit var spinnerAdapter: ArrayAdapter<GalleryType>
    private lateinit var selectedGallaryType: GalleryType
    private var filePaths: ArrayList<String> = arrayListOf();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery_request)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "New Gallery"

        FuelManager.instance.basePath = "http://api.jmlcleaningservices.com.au"

        "/gallery/types?token=${SP.getString(this, SP.TOKEN, "")}"
                .httpGet().responseObject(GalleryTypeProvider.Deserializer()) { _, response, result ->
                    val (galleryTypes, error) = result;

                    galleryTypes?.forEach {
                        galleryTypeLists.add(GalleryType(it.id, it.type, it.description))
                    }
                    galleryTypeLists.add(GalleryType(-1, "Other", ""))

                    spinnerAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, galleryTypeLists)
                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

//                    with(galleryType)
//                    {
//                        adapter = spinnerAdapter
//                        setSelection(0, false)
//                        onItemSelectedListener = this@GalleryRequestActivity
//                        prompt = "Select gallery type"
//                        gravity = Gravity.CENTER
//                    }
                }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        showToast(message = "Nothing selected")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        selectedGallaryType = galleryTypeLists[position]
        showToast(message = "Spinner 1 Position:${position} and language: ${galleryTypeLists[position]}")
    }

    private fun showToast(context: Context = applicationContext, message: String, duration: Int = Toast.LENGTH_LONG) {
        Toast.makeText(context, message, duration).show()
    }

    fun filePickerClickHandler(view: View){
        val intent = Intent()
                .setType("*/*")
                .setAction(Intent.ACTION_GET_CONTENT)
                .putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)

        startActivityForResult(Intent.createChooser(intent, "Select a images"), 111)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 111 && resultCode == RESULT_OK) {
            val selectedFile = data?.data //The uri with the location of the file
            println(data?.data);
            /*Fuel.upload("/galleries").source{
                request, url ->
                File.createTempFile("test", ".png")
            }.responseString {
                request, response, result ->
            }*/
            Fuel.upload("/galleries", Method.POST)
//                    .request.add{ DataPart(File.createTempFile("test", ".png")) }

//                    .dataParts {}
//                    .dataParts { _, _ -> listOf(DataPart(data?.data, "file", "image/*")) }

        }
    }

    fun upload(fileUri : Uri){

        val requestBody = RequestBody.create(MultipartBody.FORM, "${selectedGallaryType.id}")
        val filePart = MediaType.parse(contentResolver.getType(fileUri))

        val builder = Retrofit.Builder()
                .baseUrl("http://api.jmlcleaningservices.com.au/galleries")
                .addConverterFactory(GsonConverterFactory.create())

        val retrofit = builder.build()

        val galleryTypeService = retrofit.create(GalleryTypeService::class.java)

//        val call = galleryTypeService.store(1, filePart)
//        val result = call.execute().body()
//        val albums = AlbumMapper().transform(result.topAlbums.albums)
    }

}
