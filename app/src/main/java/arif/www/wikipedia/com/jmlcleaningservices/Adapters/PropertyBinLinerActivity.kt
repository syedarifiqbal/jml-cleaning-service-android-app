package arif.www.wikipedia.com.jmlcleaningservices.Adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import arif.www.wikipedia.com.jmlcleaningservices.NetworkModels.BinLinerSettingProvider
import arif.www.wikipedia.com.jmlcleaningservices.R
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.mapError

import kotlinx.android.synthetic.main.activity_property_bin_liner.*

class PropertyBinLinerActivity : AppCompatActivity() {

    var items: ArrayList<Int> = ArrayList<Int>();
    var quantities: ArrayList<EditText> = ArrayList<EditText>();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_bin_liner)
        setSupportActionBar(toolbar)
        progressBar.visibility = View.VISIBLE;
        formContainer.visibility = View.GONE

        val propertyId = SP.getInt(this, SP.CURRENT_PROPERTY_ID, 0);

        "/bin-liners-settings?token=${SP.getString(this, SP.TOKEN, "")}"
                .httpGet().responseObject(BinLinerSettingProvider.Deserializer()) { _, response, result ->
                    val (binLinersSettings, error) = result;
                    if(binLinersSettings?.size!! > 0) {
                        var index = 2;
                        binLinersSettings?.forEach { s ->
                            items.add(s.id)
                            val li = applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                            val row = li.inflate(R.layout.bin_liner_form_item, null);
                            val input = row.findViewById<EditText>(R.id.bin_liner_edit_text)
                            quantities.add(input);
                            val binLineName = row.findViewById<TextView>(R.id.bin_liner_name)
                            binLineName.text = s.name
                            formContainer.addView(row, index)
                            index++
                        }
                        progressBar.visibility = View.GONE
                        formContainer.visibility = View.VISIBLE
                    }
                }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun SaveLinerSetting(v: View)
    {
        val notesText = notes.text

        if(notesText!!.trim().length == 0)
        {
            notes.setError("Notes must be provided.");
            notes.requestFocus();
            return;
        }

        val tempItems = ArrayList<Pair<Int, Int>>();
        var index = 0;
        quantities.forEach{q ->
            val itemId = items.get(index)
            val qty = q.text.toString().toIntOrNull();
            if(qty !== null)
            {
                tempItems.add(Pair(itemId, qty))
                index++
            }
        }

        if(tempItems.size == 0)
        {
            Toast.makeText(this, "Please add item quantity!", Toast.LENGTH_LONG).show();
            return;
        }

        progressBar.visibility = View.VISIBLE;
        formContainer.visibility = View.GONE

        "/bin-liners?token=${SP.getString(this, SP.TOKEN,"")}"
                .httpPost(listOf("notes" to notesText, "items" to tempItems, "property_id" to SP.getInt(this, SP.CURRENT_PROPERTY_ID, 0)))
                .response { request, response, result ->
                    val (content, error) = result
                    print(error?.message);
                    print(content.toString());
                    Toast.makeText(this, "Successfully Created!", Toast.LENGTH_LONG).show();
                    progressBar.visibility = View.GONE
                    formContainer.visibility = View.VISIBLE
                    finish();
                }
                .responseString{ s ->
                    progressBar.visibility = View.GONE
                    formContainer.visibility = View.VISIBLE
                }

    }

}

class BinLinerRequestItem(val binLinerItem: Int, val qty: Int) {}