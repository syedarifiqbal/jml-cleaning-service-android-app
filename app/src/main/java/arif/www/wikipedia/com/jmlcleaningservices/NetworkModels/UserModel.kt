package arif.www.wikipedia.com.jmlcleaningservices.NetworkModels

import arif.www.wikipedia.com.jmlcleaningservices.Models.User
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class UserModel(val user: User, val token: String) {

    class Deserializer : ResponseDeserializable<UserModel>{
        override fun deserialize(content: String): UserModel? = Gson().fromJson(content, UserModel::class.java);
    }

}
