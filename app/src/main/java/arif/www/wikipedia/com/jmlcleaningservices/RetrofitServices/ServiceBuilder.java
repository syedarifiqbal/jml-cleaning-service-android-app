package arif.www.wikipedia.com.jmlcleaningservices.RetrofitServices;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceBuilder {

    private static final String URL = "http://api.jmlcleaningservices.com.au/";
//    private static final String URL = "http://10.0.2.2:1000/galleries/";

    // Create Logger
    private static HttpLoggingInterceptor logger = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    // Create okHttp Client
    private static OkHttpClient.Builder okHttp =
            new OkHttpClient.Builder()
                    // .addInterceptor(logger)
            ;

    private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttp.build());

    private static Retrofit retrofit = builder.build();

    public static <T> T buildService(Class<T> serviceType){
        return retrofit.create(serviceType);
    }
}
