package arif.www.wikipedia.com.jmlcleaningservices

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP
import com.google.gson.Gson
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.Style

class PropertyMapActivity : AppCompatActivity() {

    private var mapView: MapView? = null
    var location = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_map)

        location = SP.getString(this, SP.CURRENT_PROPERTY_LOCATION, "");

        initMap(savedInstanceState)
    }

    private fun initMap(savedInstanceState: Bundle?) {
        val locationObj = Gson().fromJson(location, PropertyDetailActivity.LocationModel::class.java);
        mapView = findViewById<MapView>(R.id.mapView)
        mapView?.onCreate(savedInstanceState)
        mapView?.getMapAsync { mapboxMap ->
            mapboxMap.addMarker(MarkerOptions()
                    .position(LatLng(locationObj.lat, locationObj.lng))
                    .title(SP.getString(this, SP.CURRENT_PROPERTY_CLIENT_NAME, "")))
            val position = CameraPosition.Builder()
                    .target(LatLng(locationObj.lat, locationObj.lng))
                    .zoom(12.0)
                    .tilt(20.0)
                    .build()
            mapboxMap.cameraPosition = position

            mapboxMap.setStyle(Style.MAPBOX_STREETS) {

            }
        }
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    /*override fun onSaveInstanceState(outState: Bundle){
        super.onSaveInstanceState(outState)
        mapView?.onSaveInstanceState(outState)
    }*/

    class LocationModel(val lat: Double, val lng: Double) {}
}
