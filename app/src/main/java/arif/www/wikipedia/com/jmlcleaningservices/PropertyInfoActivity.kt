package arif.www.wikipedia.com.jmlcleaningservices

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.view.View
import arif.www.wikipedia.com.jmlcleaningservices.NetworkModels.PropertySingleProvider
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP
import com.github.kittinunf.fuel.httpGet

import kotlinx.android.synthetic.main.activity_property_info.*
import kotlinx.android.synthetic.main.content_property_info.*
import android.content.Context
import android.widget.TextView
import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.content.Intent
import android.support.v4.app.DialogFragment
import android.support.v4.view.ViewPager
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.Toast
import arif.www.wikipedia.com.jmlcleaningservices.Adapters.PopupGalleryPagerAdapter
import arif.www.wikipedia.com.jmlcleaningservices.Models.GalleryImage
import kotlinx.android.synthetic.main.popup_gallery_view_pager.*

//import com.kodmap.app.library.PopopDialogBuilder


class PropertyInfoActivity : AppCompatActivity() {

    private lateinit var contactPhoneNumber: String
    private lateinit var contactEmailAddress: String

    private lateinit var councilPhoneNumber: String
    private lateinit var councilEmailAddress: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_info)
        setSupportActionBar(toolbar)

        progressBar.visibility = View.VISIBLE
        infoCard.visibility = View.GONE
        councilContainer.visibility = View.GONE
        contactContainer.visibility = View.GONE
        servicesContainer.visibility = View.GONE
        binContainer.visibility = View.GONE
        videoContainer.visibility = View.GONE
        galleriesContainer.visibility = View.GONE

        /*val body = listOf("key" to "value")
        Fuel.upload("/path", Method.POST, parameters = body)
                .add( FileDataPart(File()) )*/

        "/properties/${SP.getInt(this, SP.CURRENT_PROPERTY_ID, 0)}?token=${SP.getString(this, SP.TOKEN, "")}"
                .httpGet().responseObject( PropertySingleProvider.Deserializer()) { _, response, result ->
                    val (property , error) = result
                    setContentData(property)
                }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setContentData(property: PropertySingleProvider?) {
        clientName.text = property?.client?.name
        address.text = SP.getString(this, SP.CURRENT_PROPERTY_ADDRESS, "")
        progressBar.visibility = View.GONE;
        infoCard.visibility = View.VISIBLE

        if (property?.council != null)
        {
            councilPhoneNumber = property.council.phone
            councilEmailAddress = property.council.email
            councilName.text = property.council.name
            councilEmail.text = "Email: ${property.council?.email}"
            councilPhone.text = "Phone: ${property.council?.phone}"
            councilWebsite.text = "Website: ${property.council?.website}"
            councilAddress.text = "Address: ${property.council?.address}, ${property?.council?.address_state}, ${property?.council?.address_suburb}, ${property?.council?.address_post_code}"
            councilContainer.visibility = View.VISIBLE;
        }

        if (property?.contact != null)
        {
            contactPhoneNumber = property.contact.phone
            contactEmailAddress = property.contact.email
            contactName.text = "${property.contact.contact_name} ${property.contact.surname}"
            contactEmail.text = "Email: ${property.contact.email}"
            contactPhone.text = "Phone: ${property.contact.phone}"
            contactRole.text = "Role: ${property.contact.role}"
            councilContactName.text = "Contact name: ${property.contact.contact_name}"
            contactContainer.visibility = View.VISIBLE;
        }

        if(property?.services != null && property.services.size > 0)
        {
            services.text = ""
            var i = 0
            property.services.forEach{
                if(i++ > 0) { services.text = "${services.text}\n" }
                services.text = "${services.text}${it.name}"
            }
            servicesContainer.visibility = View.VISIBLE
        }
        if(property?.bin_types!!.size > 0)
        {
            var i = 1;
            property.bin_types.forEach {
                val li = applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val propertyBinRow = li.inflate(R.layout.property_bin_row, null);

                val binType = propertyBinRow.findViewById<TextView>(R.id.name);
                binType.text = it.type

                val size = propertyBinRow.findViewById<TextView>(R.id.size);
                size.text = "Quantity: ${it.pivot.qty}";

                /*val description = propertyBinRow.findViewById<TextView>(R.id.description);
                binType.text = it.description;*/

                val color = propertyBinRow.findViewById<TextView>(R.id.color);
                color.text = it.color;

                color.setBackgroundColor(Color.parseColor(it.color));
                val textColor: String
                if(isColorDark(Color.parseColor(it.color)))
                {
                    textColor = "#FFFFFF";
                }else{
                    textColor = "#000000";
                }
                color.setTextColor(Color.parseColor(textColor))
                binContainer.addView(propertyBinRow, i);
                binContainer.visibility = View.VISIBLE
                i++
            }
        }
        if(property.videos.isNotEmpty())
        {
            var i = 1
            property.videos.forEach{
                val li = applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val videoRow = li.inflate(R.layout.property_video_row, null)
                val name = videoRow.findViewById<TextView>(R.id.name)
                name.text = it.title
                val description = videoRow.findViewById<TextView>(R.id.description)
                description.text = it.description
                val url = videoRow.findViewById<TextView>(R.id.url)
                url.text = it.url
//                url.movementMethod = LinkMovementMethod.getInstance()
//                rowContainer.addView(videoRow, i);
                videoContainer.addView(videoRow, i);
                videoContainer.visibility = View.VISIBLE
                i++
            }

            println("property Videos: " + property.videos.get(0).title)
        }
        if(property.galleries.isNotEmpty())
        {
            var i = 1
            property.galleries.forEach{
                val li = applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val row = li.inflate(R.layout.property_video_row, null)
                val name = row.findViewById<TextView>(R.id.name)
                name.text = it.name
                val description = row.findViewById<TextView>(R.id.description)
                description.text = it.description
                val url = row.findViewById<TextView>(R.id.url)
                url.text = ""

                row.setOnClickListener { v->
                    run {

                        val d = Dialog(this);
                        d.setTitle("Gallery Images");
                        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        d.setContentView(R.layout.popup_gallery_view_pager);
//                        d.setCanceledOnTouchOutside(false);
                        d.setCancelable(true);

                        val url_list = ArrayList<GalleryImage>()

                        it.images.forEach {
                            url_list.add(GalleryImage(it.id, "http://lorempixel.com/400/200/sports/", ""));
                            url_list.add(GalleryImage(it.id, "http://lorempixel.com/400/200/cat/", ""));
                            url_list.add(GalleryImage(it.id, "http://lorempixel.com/400/200/abstract/", ""));
                            url_list.add(GalleryImage(it.id, "http://lorempixel.com/400/200/food/", ""));
                            url_list.add(GalleryImage(it.id, "http://lorempixel.com/400/200/technics/", ""));
                        }

                        val pager = d.findViewById<ViewPager>(R.id.galleryPager);
                        val adapter = PopupGalleryPagerAdapter(this, url_list);
                        pager.adapter = adapter;

                        d.show();

//                        galleryPager.set


//                        var dialog = PopopDialogBuilder(this)
//                                // Set list like as option1 or option2 or option3
//                                .setList(url_list)
//                                // or setList with initial position that like .setList(list,position)
//                                // Set dialog header color
//                                .setHeaderBackgroundColor(android.R.color.holo_blue_light)
//                                // Set dialog background color
//                                .setDialogBackgroundColor(R.color.color_dialog_bg)
//                                // Set close icon drawable
//                                .setCloseDrawable(R.drawable.ic_close_white_24dp)
//                                // Set loading view for pager image and preview image
//                                .setLoadingView(R.layout.loading_view)
//                                // Set dialog style
////                                .setDialogStyle(R.style.DialogStyle)
//                                // Choose selector type, indicator or thumbnail
//                                .showThumbSlider(true)
//                                // Set image scale type for slider image
//                                .setSliderImageScaleType(ImageView.ScaleType.FIT_XY)
//                                // Set indicator drawable
////                                .setSelectorIndicator(R.drawable.sample_indicator_selector)
//                                // Enable or disable zoomable
//                                .setIsZoomable(true)
//                                // Build Km Slider Popup Dialog
//                                .build()
//
//                        dialog.show();

                        // Toast.makeText(this, "Working on this part to show images on click.", Toast.LENGTH_LONG).show();
                    }
                }

                // insert row to container
                galleriesContainer.addView(row, i);
                galleriesContainer.visibility = View.VISIBLE
                i++
            }
        }
    }

    fun isColorDark(color: Int): Boolean {
        val darkness = 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255
        return darkness >= 0.5
    }

    fun onPhoneClickHandler(v:View) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:${contactPhoneNumber}")
        startActivity(intent)
    }

    fun onEmailClickHandler(v:View) {
        val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",contactEmailAddress, null));
        // intent.setType("message/rfc822");
//        intent.putExtra(Intent.EXTRA_EMAIL, contactEmailAddress)
        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject Will Goes Here!")
        intent.putExtra(Intent.EXTRA_TEXT,  "You can ask me to change the subject or you let me know if you want me to remove subject by default, also if you want to have anything in body by default or it can be removed.")
        startActivity(Intent.createChooser(intent, "Send Email"));
    }

    fun onCouncilPhoneClickHandler(v:View) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:${councilPhoneNumber}")
        startActivity(intent)
    }

    fun onCouncilEmailClickHandler(v:View) {
        val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",councilEmailAddress, null));
        /*intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, contactEmailAddress)*/
        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject Will Goes Here!")
        intent.putExtra(Intent.EXTRA_TEXT,  "You can ask me to change the subject or you let me know if you want me to remove subject by default, also if you want to have anything in body by default or it can be removed.")
        startActivity(Intent.createChooser(intent, "Send Email"));
    }

}
