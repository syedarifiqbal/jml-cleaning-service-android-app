//package arif.www.wikipedia.com.jmlcleaningservices.Adapters
//
//import android.R
//import android.content.Context
//import android.support.v4.view.PagerAdapter
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.RelativeLayout
//import android.widget.Toast
//
//import com.squareup.picasso.Callback
//import com.squareup.picasso.Picasso
//
//import arif.www.wikipedia.com.jmlcleaningservices.Models.GalleryImage
//
//class GalleryPagerAdapter(private val context: Context, private val images: List<GalleryImage>) : PagerAdapter() {
//    private val layoutInflater: LayoutInflater
//
//    init {
//        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//    }
//
//    override fun getCount(): Int {
//        return images.size
//    }
//
//    override fun isViewFromObject(view: View, `object`: Any): Boolean {
//        return view === `object`
//    }
//
//    override fun instantiateItem(container: ViewGroup, position: Int): Any {
////        val itemView = layoutInflater.inflate(R.layout.fragment, container, false)
//        val image = images[position]
//        val imageView = itemView.findViewById(R.id.sliderImage)
//        val loadingbar = itemView.findViewById(R.id.loadingbar)
//
//        //        textView.setText(image.getUrl());
//        //        imageView.setImageResource(image.getId());
//        Picasso.get()
//                .load("http://jmlcleaningservices.com.au/jml/uploads/gallery/" + image.image)
//                //                .resize(600, 600)
//                .fit()
//                .centerInside()
//                .into(imageView, object : Callback {
//                    override fun onSuccess() {
//                        loadingbar.setVisibility(View.GONE)
//                    }
//
//                    override fun onError(e: Exception) {
//                        Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
//                    }
//                })
//        container.addView(itemView)
//        return itemView
//        // return super.instantiateItem(container, position);
//    }
//
//    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
//        // super.destroyItem(container, position, object);
//        container.removeView(`object` as RelativeLayout)
//    }
//}
