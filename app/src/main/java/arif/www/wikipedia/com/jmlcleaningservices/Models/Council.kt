package arif.www.wikipedia.com.jmlcleaningservices.Models

class Council(
        val id: Int,
        val name: String,
        val phone: String,
        val email: String,
        val website: String,
        val address: String,
        val address_state: String,
        val address_long_state: String,
        val address_suburb: String,
        val address_post_code: String,
        val address_location: String,
        val active: Boolean,
        val contact_name: String,
        val tender: String,
        val waste_department: String
) {

}