package arif.www.wikipedia.com.jmlcleaningservices.NetworkModels

import arif.www.wikipedia.com.jmlcleaningservices.Models.*
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class PropertySingleProvider(
        val id: Int,
        val client_id: Int,
        val council_id: Int,
        val contact_id: Int,
        val strata_plan: String,
        val notes: String,
        val address: String,
        val address_state: String,
        val address_long_state: String,
        val address_suburb: String,
        val address_post_code: String,
        val address_location: String,
//        val active: Boolean,
        val client: Client,
        val galleries: List<Gallery>,
        val council: Council,
        val contact: Contact,
        val services: List<Service>,
        val bin_types: List<BinType>,
        val keys: List<KeyType>,
        val videos: List<Video>) {

    class Deserializer : ResponseDeserializable<PropertySingleProvider>{
        override fun deserialize(content: String): PropertySingleProvider? = Gson().fromJson(content, PropertySingleProvider::class.java);
    }

}
