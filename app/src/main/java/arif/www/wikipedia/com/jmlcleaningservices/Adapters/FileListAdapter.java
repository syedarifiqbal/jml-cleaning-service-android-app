package arif.www.wikipedia.com.jmlcleaningservices.Adapters;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;

import java.util.ArrayList;

import arif.www.wikipedia.com.jmlcleaningservices.GalleryRequestJavaActivity;
import arif.www.wikipedia.com.jmlcleaningservices.R;
import arif.www.wikipedia.com.jmlcleaningservices.Utils.FileUtils;

public class FileListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Uri> arrayList;
    private ArrayList<String> filesPortraitInfo;

    public FileListAdapter(Context context, ArrayList<Uri> arrayList, ArrayList<String> filesPortraitInfo) {
        this.context = context;
        this.arrayList = arrayList;
        this.filesPortraitInfo = filesPortraitInfo;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (mInflater != null) {
            convertView = mInflater.inflate(R.layout.list_items, null);
        }

        ImageView imageView = convertView.findViewById(R.id.imageView);
        final TextView imagePath = convertView.findViewById(R.id.imagePath);
        ImageButton deleteBtn = convertView.findViewById(R.id.delete_btn);

        String p = FileUtils.getPath(context, arrayList.get(position));
        final String[] pathParts = p.split("/");

        imagePath.setText(pathParts[pathParts.length-1]);

        Glide.with(context)
                .load(arrayList.get(position))
                .into(imageView);

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context)
                        .setTitle("Confirmation")
                        .setMessage("Do you really want to remove this image?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                arrayList.remove(position);
                                filesPortraitInfo.remove(position);
                                notifyDataSetChanged();
                                Toast.makeText(context, pathParts[pathParts.length-1] + " is removed from upload list", Toast.LENGTH_LONG).show();
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });

        return convertView;
    }
}
