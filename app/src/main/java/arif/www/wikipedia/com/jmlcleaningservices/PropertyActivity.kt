package arif.www.wikipedia.com.jmlcleaningservices

import android.app.SearchManager
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.View
import android.widget.SearchView
import android.widget.Toast
import arif.www.wikipedia.com.jmlcleaningservices.Adapters.PropertyRecyclerAdapter
import arif.www.wikipedia.com.jmlcleaningservices.NetworkModels.PropertyProvider
import arif.www.wikipedia.com.jmlcleaningservices.Models.Property
import arif.www.wikipedia.com.jmlcleaningservices.Utils.Helpers
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.httpGet
import kotlinx.android.synthetic.main.activity_properties.*

class PropertyActivity : AppCompatActivity() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    val propertiesList = ArrayList<Property>();
    private lateinit var adapter: PropertyRecyclerAdapter;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_properties)

        FuelManager.instance.basePath = "http://api.jmlcleaningservices.com.au"

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()?.setDisplayShowHomeEnabled(true);

        linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        clientRecyclerView.layoutManager = linearLayoutManager

        showProgressbar(false)
        adapter = PropertyRecyclerAdapter(this, propertiesList);
        clientRecyclerView.adapter = adapter;

        /*"/properties?token=${SP.getString(this, SP.TOKEN, "")}"
                .httpGet().responseObject(PropertyProvider.Deserializer()) { _, response, result ->
                    val (PropertyProvider, error) = result;
                    *//*println(PropertyProvider.toString());
                    println(response.statusCode);
                    println(response.url);*//*

                    PropertyProvider?.data?.forEach { propertiesList.add(it); }

                }*/
        /*"/properties?token=${SP.getString(this, SP.TOKEN,"")}"
                .httpGet().responseString() { _, response, result ->
            val (data, error) = result;
                    val s = Gson().fromJson(data, PropertyProvider::class.java)
            println(data);
            println(s);
            println(response.statusCode);
            println(response.url);
        }*/

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_only, menu);

        val searchItem = menu?.findItem(R.id.search)
        val searchView = searchItem?.actionView as SearchView
        searchView.setQueryHint(getString(R.string.property_search_hint))

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                if(newText.trim().isEmpty())
                {
                    adapter.properties = listOf();
                    adapter.notifyDataSetChanged();
                    noteMessage.visibility = View.VISIBLE;
                    return false
                }
                noteMessage.visibility = View.GONE;
                doSearch(newText);
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // doSearch(query)
                return false
            }
        })

        searchView.setOnCloseListener(
                SearchView.OnCloseListener {
                    adapter.properties = propertiesList;
                    adapter.notifyDataSetChanged();
                    false;
                }
        )

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu?.findItem(R.id.search)?.actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
        }

        return true;
    }

    private fun doSearch(query: String) {
        if(!Helpers.checkConnection(this))
        {
            Toast.makeText(this, "Make sure you have working connection!", Toast.LENGTH_LONG).show();
            return;
        }
        showProgressbar(true);
        "/properties/term?q=${query}&token=${SP.getString(this, SP.TOKEN, "")}"
                .httpGet().responseObject(PropertyProvider.Deserializer()) { _, response, result ->
                    val (PropertyProvider, error) = result;
                    showProgressbar(false)
                    println(result)
                    adapter.properties = PropertyProvider!!.data
                    adapter.notifyDataSetChanged();
                    showProgressbar(false);
                    clientRecyclerView.adapter = adapter;
                }
    }

    fun showProgressbar(isVisitble: Boolean) {
        if (isVisitble) {
            progressBar.setVisibility(View.VISIBLE);
            clientRecyclerView.visibility = View.GONE;
            return;
        }
        progressBar.setVisibility(View.GONE);
        clientRecyclerView.visibility = View.VISIBLE;
    }
}
