package arif.www.wikipedia.com.jmlcleaningservices.NetworkModels

import arif.www.wikipedia.com.jmlcleaningservices.Models.*
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class ClientProvider(
        val id: Int,
        val name: String,
        val client_type: Int,
        val lead_type: Int,
        val phone: String,
        val email: String,
        val username: String,
        val password: String,
        val website: String,
        val strata_plan: String,
        val is_parent: Boolean,
        val is_prospect: Boolean,
        val child_of: Int,
        val parent: Client,
        val address_1: String,
        val address_2: String,
        val address_state: String,
        val address_long_state: String,
        val address_suburb: String,
        val address_post_code: String,
        val address_location: String,
        val same_billing_address: String,
        val attention: String,
        val role: Int,
        val co: String,
        val billing_address_1: String,
        val billing_address_2: String,
        val address_street: String,
        val billing_address_street: String,
        val billing_state: String,
        val billing_long_state: String,
        val billing_suburb: String,
//        val active: Boolean,
        val billing_post_code: String,
        val billing_address_location: String,
        val properties: Property) {

    class Deserializer : ResponseDeserializable<ClientProvider>{
        override fun deserialize(content: String): ClientProvider? = Gson().fromJson(content, ClientProvider::class.java);
    }

}
