package arif.www.wikipedia.com.jmlcleaningservices;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import arif.www.wikipedia.com.jmlcleaningservices.Adapters.FileListAdapter;
import arif.www.wikipedia.com.jmlcleaningservices.Models.GalleryType;
import arif.www.wikipedia.com.jmlcleaningservices.RetrofitServices.GalleryTypeService;
import arif.www.wikipedia.com.jmlcleaningservices.RetrofitServices.ServiceBuilder;
import arif.www.wikipedia.com.jmlcleaningservices.Utils.FileUtils;
import arif.www.wikipedia.com.jmlcleaningservices.Utils.Helpers;
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP;
import arif.www.wikipedia.com.jmlcleaningservices.Utils.Settings;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static arif.www.wikipedia.com.jmlcleaningservices.Utils.FileUtils.isLocal;

public class GalleryRequestJavaActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 111;
    private static final String TAG = "UPLOADER";
    private ArrayList<GalleryType> galleryTypeLists = new ArrayList<GalleryType>();
    private ArrayAdapter<GalleryType> spinnerAdapter;
    private GalleryType selectedGallaryType;
    private Spinner spinner;
    private ListView listView;
    private ArrayList<Uri> arrayList;
    private ArrayList<String> filePortraitInfo;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 124;
    private ProgressBar mProgressBar;
    private NestedScrollView wrapper;
    private LinearLayout otherGalleryWrapper;
    private EditText otherGalleryType;
    private EditText otherGalleryDescription;
    private int propertyId;
    private String location;
    private String clientName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_request_java);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Request Gallery");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spinner = findViewById(R.id.galleryType);
        listView = findViewById(R.id.listView);
        mProgressBar = findViewById(R.id.progressBar);
        wrapper = findViewById(R.id.wrapper);
        wrapper.setNestedScrollingEnabled(true);

        otherGalleryWrapper = findViewById(R.id.otherGalleryWrapper);
        otherGalleryType = findViewById(R.id.otherGalleryType);
        otherGalleryDescription = findViewById(R.id.otherGalleryDescription);
        arrayList = new ArrayList<>();
        filePortraitInfo = new ArrayList<>();
        propertyId = SP.getInt(this, SP.CURRENT_PROPERTY_ID, 0);
        clientName = SP.getString(this, SP.CURRENT_PROPERTY_CLIENT_NAME,"");
        location = SP.getString(this, SP.CURRENT_PROPERTY_LOCATION, "");

        /*Log.d("PROPERTY_DETAIL_CONST","LOCATION " + PropertyDetailActivity.Companion.getLocation());
        Log.d("PROPERTY_DETAIL_CONST","CLIENT_NAME " + PropertyDetailActivity.Companion.getClientName());
        Log.d("PROPERTY_DETAIL_CONST","PROPERTY_ID " + PropertyDetailActivity.Companion.getPropertyId());*/
        showProgress();
        GalleryTypeService galleryTypeService = ServiceBuilder.buildService(GalleryTypeService.class);
        Call<JsonArray> call = galleryTypeService.get(SP.getString(this, SP.TOKEN, ""));

        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                if (response.body() != null) {
                    // Log.d("RESPONSE", response.body().toString());
                    JsonArray types = response.body();
                    for (int i=0; i<types.size(); i++)
                    {
                        JsonObject type = types.get(i).getAsJsonObject();
                        GalleryType galleryType = new GalleryType(
                                type.get("id").getAsInt(),
                                type.get("type").getAsString(),
                                type.get("description").isJsonNull()? "": type.get("description").getAsString());

                        galleryTypeLists.add(galleryType);
                    }
                    /*galleryTypeLists.add(new GalleryType(-1, "Other", ""));*/

                    spinnerAdapter = new ArrayAdapter<GalleryType>(GalleryRequestJavaActivity.this, android.R.layout.simple_spinner_item, galleryTypeLists);
                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    spinner.setAdapter(spinnerAdapter);

                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            selectedGallaryType = galleryTypeLists.get(position);
                            if(selectedGallaryType.getType().equalsIgnoreCase("other")){
                                otherGalleryWrapper.setVisibility(View.VISIBLE);
                                return;
                            }
                            otherGalleryWrapper.setVisibility(View.GONE);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    hideProgress();
                }else{
                    Toast.makeText(GalleryRequestJavaActivity.this, response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Log.d("RESPONSE", t.getLocalizedMessage());
                // Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });
    }

    private void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        wrapper.setVisibility(View.GONE);
        listView.setVisibility(View.GONE);
    }

    private void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
        wrapper.setVisibility(View.VISIBLE);
        listView.setVisibility(View.VISIBLE);
    }

    public void filePickerClickHandler(View view) {
        if (askForPermission())
            showChooser();
    }

    private void showChooser() {
        // Use the GET_CONTENT intent from the utility class
        // Intent target = FileUtils.createGetContentIntent();
        // Create the chooser Intent
        // Intent intent = Intent.createChooser(target, getString(R.string.chooser_title));
        Intent intent = new Intent()
                .setType("image/*")
                .setAction(Intent.ACTION_GET_CONTENT)
                .putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        try {
            startActivityForResult(Intent.createChooser(intent, getString(R.string.chooser_title)), REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            // The reason for the existence of aFileChooser
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE:
                // If the file selection was successful
                if (resultCode == RESULT_OK) {
                    // Check if multiple images are selected
                    if(data.getClipData() != null) {
                        int count = data.getClipData().getItemCount();
                        int currentItem = 0;
                        while(currentItem < count) {
                            Uri imageUri = data.getClipData().getItemAt(currentItem).getUri();
                            currentItem = currentItem + 1;
                            Log.d("Uri Selected", imageUri.toString());
                            try {
                                // Get the file path from the URI
                                String path = imageUri.getPath();
                                Log.d("Multiple File Selected", path);
                                arrayList.add(imageUri);
                                filePortraitInfo.add(isPortrait(imageUri));
                                FileListAdapter mAdapter = new FileListAdapter(GalleryRequestJavaActivity.this, arrayList, filePortraitInfo);
                                listView.setAdapter(mAdapter);
                            } catch (Exception e) {
                                Log.e(TAG, "File select error", e);
                            }
                        }
                    } else if(data.getData() != null) {
                        //do something with the image (save it to some directory or whatever you need to do with it here)
                        final Uri uri = data.getData();

                        Log.i(TAG, "Uri = " + uri.toString());
                        try {
                            // Get the file path from the URI
                            final String path = uri.getPath();
                            Log.d("Single File Selected", path);
                            arrayList.add(uri);
                            filePortraitInfo.add(isPortrait(uri));
                            FileListAdapter mAdapter = new FileListAdapter(GalleryRequestJavaActivity.this, arrayList, filePortraitInfo);
                            listView.setAdapter(mAdapter);

                        } catch (Exception e) {
                            Log.e(TAG, "File select error", e);
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void uploadImagesToServer() {
        showProgress();
        if (!validateFields())
            return;

        if (!Helpers.checkConnection(GalleryRequestJavaActivity.this)) {
            hideProgress();
            Toast.makeText(GalleryRequestJavaActivity.this,
                    "Make sure you have working internet connection", Toast.LENGTH_SHORT).show();
            return;
        }
        /*Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:1000/galleries/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();*/
        // create list of file parts (photo)
        List<MultipartBody.Part> parts = new ArrayList<>();
        // create upload service client
        GalleryTypeService service = ServiceBuilder.buildService(GalleryTypeService.class);
        if (arrayList != null) {
            // create part for file (photo)
            for (int i = 0; i < arrayList.size(); i++) {
                parts.add(prepareFilePart("image"+i, arrayList.get(i)));
            }
        }
        // create a map of data to pass along
        RequestBody description = createPartFromString(otherGalleryDescription.getText().toString());
        RequestBody property_id = createPartFromString(propertyId+"");
        RequestBody type = createPartFromString(otherGalleryType.getText().toString());
        RequestBody galleryTypeId = createPartFromString(selectedGallaryType.getId()+"");
        RequestBody size = createPartFromString(""+parts.size());

        RequestBody isFilePortraits;
        if(Settings.Companion.getBool(this, "should_rotate_portrait")){
            isFilePortraits = createPartFromString(TextUtils.join(",", filePortraitInfo));
        }else{
            isFilePortraits = createPartFromString("");
        }
        // finally, execute the request
        Call<ResponseBody> call = service.upload(
                SP.getString(GalleryRequestJavaActivity.this, SP.TOKEN,""),
                description,
                galleryTypeId,
                property_id,
                type,
                size,
                parts,
                isFilePortraits);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                hideProgress();
//                if(response.isSuccessful()) {
                    Intent intent = new Intent();
                    intent.putExtra("location", location);
                    intent.putExtra("clientName", clientName);
                    intent.putExtra("propertyId", propertyId);
                    setResult(Activity.RESULT_OK, intent);
                    finish();

                    Toast.makeText(GalleryRequestJavaActivity.this,
                            "Images successfully uploaded!", Toast.LENGTH_SHORT).show();
                    /*Toast.makeText(GalleryRequestJavaActivity.this,
                            response.raw().toString(), Toast.LENGTH_SHORT).show();*/
//                } else {
                    Toast.makeText(GalleryRequestJavaActivity.this,
                            "Something went wrong. property id is : " + propertyId, Toast.LENGTH_SHORT).show();
//                }
            }
            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                hideProgress();
                // Snackbar.make(parentView, t.getMessage(), Snackbar.LENGTH_LONG).show();
                Toast.makeText(GalleryRequestJavaActivity.this,
                        t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String isPortrait(Uri uri){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(FileUtils.getPath(this, uri), options);
        Toast.makeText(this, "Image Width is: " + options.outWidth + "px Image Height is: " + options.outHeight, Toast.LENGTH_LONG).show();
        return options.outHeight > options.outWidth? "1": "0";
    }

    private boolean validateFields() {
        Boolean ret = true;
        if (selectedGallaryType.getType().equalsIgnoreCase("other"))
        {
            if(otherGalleryType.getText().toString().trim().length() < 4)
            {
                otherGalleryType.setError("Gallery Type must be 4 Characters long!");
                ret = false;
            }
        }

        if(arrayList.size() < 1 )
        {
            Toast.makeText(GalleryRequestJavaActivity.this,
                    "Please Choose images to upload", Toast.LENGTH_SHORT).show();
            ret = false;
        }

        return ret;
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        File xFile = FileUtils.getFile(this, fileUri);

        String filePath = FileUtils.getPath(this, fileUri);

        String f = resizeAndCompressImageBeforeSend(filePath, xFile.getName());

        File file = new File(f);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(Objects.requireNonNull(getContentResolver().getType(fileUri))),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    public String resizeAndCompressImageBeforeSend(String filePath, String fileName){
        final int MAX_IMAGE_SIZE = 100 * 1024; // max final file size in kilobytes

        Log.d("compressBitmap","Start Compression");

        // First decode with inJustDecodeBounds=true to check dimensions of image
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath,options);

        // Calculate inSampleSize(First we are going to resize the image to 800x800 image, in order to not have a big but very low quality image.
        //resizing the image will already reduce the file size, but after resizing we will check the file size and start to compress image
        options.inSampleSize = calculateInSampleSize(options, 400, 400);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        options.inPreferredConfig= Bitmap.Config.ARGB_8888;

        Bitmap bmpPic = BitmapFactory.decodeFile(filePath,options);


        int compressQuality = 60; // quality decreasing by 5 every loop.
        int streamLength;
        do{
            ByteArrayOutputStream bmpStream = new ByteArrayOutputStream();
            Log.d("compressBitmap", "Quality: " + compressQuality);
            bmpPic.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream);
            byte[] bmpPicByteArray = bmpStream.toByteArray();
            streamLength = bmpPicByteArray.length;
            compressQuality -= 5;
            Log.d("compressBitmap", "Size: " + streamLength/1024+" kb");
        }while (streamLength >= MAX_IMAGE_SIZE);

        try {
            //save the resized and compressed file to disk cache
            Log.d("compressBitmap","cacheDir: "+ this.getCacheDir());
            FileOutputStream bmpFile = new FileOutputStream(this.getCacheDir()+fileName);
            bmpPic.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpFile);
            bmpFile.flush();
            bmpFile.close();
        } catch (Exception e) {
            Log.e("compressBitmap", "Error on saving file");
        }
        //return the path of resized and compressed file
        return  this.getCacheDir()+fileName;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        String debugTag = "MemoryInformation";
        // Image nin islenmeden onceki genislik ve yuksekligi
        final int height = options.outHeight;
        final int width = options.outWidth;
        Log.d(debugTag,"image height: "+height+ "---image width: "+ width);
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        Log.d(debugTag,"inSampleSize: "+inSampleSize);
        return inSampleSize;
    }

    /**
     *  Runtime Permission
     */
    private boolean askForPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            int hasCallPermission = ContextCompat.checkSelfPermission(GalleryRequestJavaActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
            if (hasCallPermission != PackageManager.PERMISSION_GRANTED) {
                // Ask for permission
                // need to request permission
                if (ActivityCompat.shouldShowRequestPermissionRationale(GalleryRequestJavaActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // explain
                    showMessageOKCancel(
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    ActivityCompat.requestPermissions(GalleryRequestJavaActivity.this,
                                            new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                                            REQUEST_CODE_ASK_PERMISSIONS);
                                }
                            });
                    // if denied then working here
                } else {
                    // Request for permission
                    ActivityCompat.requestPermissions(GalleryRequestJavaActivity.this,
                            new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_CODE_ASK_PERMISSIONS);
                }

                return false;
            } else {
                // permission granted and calling function working
                return true;
            }
        } else {
            return true;
        }
    }

    private void showMessageOKCancel(DialogInterface.OnClickListener okListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(GalleryRequestJavaActivity.this);
        final AlertDialog dialog = builder.setMessage("You need to grant access to Read External Storage")
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                        ContextCompat.getColor(GalleryRequestJavaActivity.this, android.R.color.holo_blue_light));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(
                        ContextCompat.getColor(GalleryRequestJavaActivity.this, android.R.color.holo_red_light));
            }
        });

        dialog.show();

    }

    public void uploadBtnHandler(View view) {
        showProgress();
        /* Delaying upload so that it shows progressbar properly.
         * */
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                uploadImagesToServer();
            }
        }, 10);
    }

    public void onBackPressed() {
        super.onBackPressed();
        setIntentResult();
    }

    private void setIntentResult() {
        Intent intent = new Intent();
        intent.putExtra("location", location);
        intent.putExtra("clientName", clientName);
        intent.putExtra("propertyId", propertyId);
        setResult(Activity.RESULT_OK, intent);
    }
}
