package arif.www.wikipedia.com.jmlcleaningservices.NetworkModels

import arif.www.wikipedia.com.jmlcleaningservices.Models.Property
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import java.util.*

data class GalleryProvider(val id: Int, val type: String, val description: String, val active: Boolean) {

    class Deserializer : ResponseDeserializable<Property>{
        override fun deserialize(content: String): Property? = Gson().fromJson(content, Property::class.java);
    }

}
