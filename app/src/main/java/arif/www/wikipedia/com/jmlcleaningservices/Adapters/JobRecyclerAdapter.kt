package arif.www.wikipedia.com.jmlcleaningservices.Adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import arif.www.wikipedia.com.jmlcleaningservices.GallerySliderActivity
import arif.www.wikipedia.com.jmlcleaningservices.Models.Gallery
import arif.www.wikipedia.com.jmlcleaningservices.Models.Job
import arif.www.wikipedia.com.jmlcleaningservices.R
import kotlinx.android.synthetic.main.gallery_list_item.view.*
import kotlinx.android.synthetic.main.job_item_date.view.*
import kotlinx.android.synthetic.main.property_list_item.view.*
import java.text.SimpleDateFormat
import java.util.*


class JobRecyclerAdapter(val context: Context, var jobs: List<Job>) : RecyclerView.Adapter<JobRecyclerAdapter.ViewHolder>()  {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobRecyclerAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.job_item_date, parent, false);
        return ViewHolder(view)
    }

    override fun getItemCount() = jobs.size

    override fun onBindViewHolder(holder: JobRecyclerAdapter.ViewHolder, position: Int) {
        val job = jobs[position]
        holder.setData(job);
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        //2
        private var view: View = v
        private var job: Job? = null

        fun setData(job: Job?) {
            this.job = job;
            view.jobTitle.text = job?.job_title;
            view.jobDescription.text = job?.instruction;

            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val date = sdf.parse(job?.visits?.get(0)?.date)
            val cal = Calendar.getInstance()
            cal.time = date

            view.date.text = SimpleDateFormat("dd MMM, yyyy").format(cal.time)
        }

        //3
        init {
            v.setOnClickListener(this)
        }

        //4
        override fun onClick(v: View) {
            /*val intent = Intent(context, GallerySliderActivity::class.java);
            intent.putExtra("GALLERY_ID", gallery?.id)
            context.startActivity(intent);*/
        }
    }

}