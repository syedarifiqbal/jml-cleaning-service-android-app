package arif.www.wikipedia.com.jmlcleaningservices.Models

import java.security.Key

class Property(
        val id: Int,
        val client_id: Int,
        val council_id: Int,
        val contact_id: Int,
        val strata_plan: String,
        val notes: String,
        val address: String,
        val address_state: String,
        val address_long_state: String,
        val address_suburb: String,
        val address_post_code: String,
        val address_location: String,
//        val active: Boolean,
        val client: Client,
        val galleries: List<Gallery>,
        val council: List<Council>,
        val contact: List<Contact>,
        val services: List<Service>,
        val bin_types: List<BinType>,
        val videos: List<Video>,
        val keys: List<Key>,
        val pivot: pivot
) {

}

class pivot( val property_id: Int, val bin_type: Int, val qty: Int ){}