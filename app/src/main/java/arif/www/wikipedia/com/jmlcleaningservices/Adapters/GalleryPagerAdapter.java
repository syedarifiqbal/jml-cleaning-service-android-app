package arif.www.wikipedia.com.jmlcleaningservices.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import arif.www.wikipedia.com.jmlcleaningservices.Models.GalleryImage;
import arif.www.wikipedia.com.jmlcleaningservices.R;

public class GalleryPagerAdapter extends PagerAdapter {

    private Context context;
    private List<GalleryImage> images;
    private LayoutInflater layoutInflater;

    public GalleryPagerAdapter(Context context, List<GalleryImage> images) {
        this.context = context;
        this.images = images;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.fragment_gallery_slider, container, false);
        GalleryImage image = images.get(position);
        ImageView imageView = itemView.findViewById(R.id.sliderImage);
        final ProgressBar loadingbar = itemView.findViewById(R.id.loadingbar);

//        textView.setText(image.getUrl());
//        imageView.setImageResource(image.getId());
        Picasso.get()
                .load("http://jmlcleaningservices.com.au/jml/uploads/gallery/"+image.getImage())
//                .resize(600, 600)
                .fit()
                .centerInside()
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        loadingbar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        container.addView(itemView);
        return itemView;
        // return super.instantiateItem(container, position);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        // super.destroyItem(container, position, object);
        container.removeView((RelativeLayout) object);
    }
}
