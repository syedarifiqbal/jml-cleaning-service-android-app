package arif.www.wikipedia.com.jmlcleaningservices.Models

class Video(
        val id: Int,
        val property_id: Int,
        val title: String,
        val description: String,
        val url: String,
        val active: Boolean
) {

}