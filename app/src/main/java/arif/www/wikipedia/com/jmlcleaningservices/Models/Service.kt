package arif.www.wikipedia.com.jmlcleaningservices.Models

class Service(
        val id: Int,
        val name: String,
        val job_type: String,
        val rate: Int,
        val image: String,
        val description: String,
        val active: Boolean
) {

}