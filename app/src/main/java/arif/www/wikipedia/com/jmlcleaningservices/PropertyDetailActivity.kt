package arif.www.wikipedia.com.jmlcleaningservices

import android.os.Bundle

import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import arif.www.wikipedia.com.jmlcleaningservices.Adapters.PropertySingleRecyclerAdapter
import arif.www.wikipedia.com.jmlcleaningservices.Models.PropertySingleItem
import com.google.gson.Gson
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.Style
import kotlinx.android.synthetic.main.activity_property_detail.*
import java.util.*
import com.mapbox.mapboxsdk.camera.CameraPosition
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.widget.TextView


class PropertyDetailActivity : AppCompatActivity() {

    private var mapView: MapView? = null
    private var adapter: PropertySingleRecyclerAdapter? = null
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var recyclerView: RecyclerView

//    companion object {
        var propertyId = 0
        var location = ""
        var clientName = ""
        var address = ""
        val REQUEST_CODE = 1001
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_detail)
        setSupportActionBar(toolbar)
        propertyId = SP.getInt(this, SP.CURRENT_PROPERTY_ID, 0);
        clientName = SP.getString(this, SP.CURRENT_PROPERTY_CLIENT_NAME,"");
        location = SP.getString(this, SP.CURRENT_PROPERTY_LOCATION, "");
        address = SP.getString(this, SP.CURRENT_PROPERTY_ADDRESS, "");
        toolbar_layout.setTitle(clientName);
        recyclerView = findViewById(R.id.ac_property_recycler);

        val clientNameTv = findViewById<TextView>(R.id.clientName)
        val addressTv = findViewById<TextView>(R.id.address)
        clientNameTv.text = clientName
        addressTv.text = address

        val collapsingToolbarLayout = findViewById(R.id.toolbar_layout) as CollapsingToolbarLayout
        val appBarLayout = findViewById(R.id.app_bar) as AppBarLayout
        appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            internal var isShow = true
            internal var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.title = clientName
                    isShow = true
                } else if (isShow) {
                    collapsingToolbarLayout.title = " " //carefull there should a space between double quote otherwise it wont work
                    isShow = false
                }
            }
        })

        initMap(savedInstanceState)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Thinking to Implement somthing here.", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        setupOptionsIcon()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initMap(savedInstanceState: Bundle?) {
        val locationObj = Gson().fromJson(location, PropertyDetailActivity.LocationModel::class.java);
        mapView = findViewById<MapView>(R.id.mapView)
        mapView?.onCreate(savedInstanceState)
        mapView?.getMapAsync { mapboxMap ->
            mapboxMap.addMarker(MarkerOptions()
                    .position(LatLng(locationObj.lat, locationObj.lng))
                    .title(clientName))
            val position = CameraPosition.Builder()
                    .target(LatLng(locationObj.lat, locationObj.lng))
                    .zoom(12.0)
                    .tilt(20.0)
                    .build()
            mapboxMap.cameraPosition = position

            mapboxMap.setStyle(Style.MAPBOX_STREETS) {

            }
        }
    }

    private fun setupOptionsIcon() {
        gridLayoutManager = GridLayoutManager(this, 2)
        recyclerView.layoutManager = gridLayoutManager

        val items = ArrayList<PropertySingleItem>();
        items.add(PropertySingleItem("Issues", R.mipmap.issues));
        items.add(PropertySingleItem("Gallery", R.mipmap.gallery));
        items.add(PropertySingleItem("Client Info", R.mipmap.property_info));
        items.add(PropertySingleItem("Security", R.mipmap.security));
        items.add(PropertySingleItem("Supplies", R.mipmap.supplies));
        items.add(PropertySingleItem("Instructions", R.mipmap.instructions));
        adapter = PropertySingleRecyclerAdapter(this, items);
        recyclerView.adapter = adapter;
    }

    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                location = data!!.getStringExtra("location")
                clientName = data.getStringExtra("clientName")
                propertyId = data.getIntExtra("propertyId", 0)
                finish();
            }
        }
    }*/

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    /*override fun onSaveInstanceState(outState: Bundle){
        super.onSaveInstanceState(outState)
        mapView?.onSaveInstanceState(outState)
    }*/

    class LocationModel(val lat: Double, val lng: Double) {}
}
