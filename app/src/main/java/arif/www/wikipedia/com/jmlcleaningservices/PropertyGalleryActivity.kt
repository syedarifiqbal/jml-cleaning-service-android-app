package arif.www.wikipedia.com.jmlcleaningservices

import android.app.Activity
import android.content.Intent
import android.opengl.Visibility
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.ProgressBar
import arif.www.wikipedia.com.jmlcleaningservices.Adapters.GalleryRecyclerAdapter
import arif.www.wikipedia.com.jmlcleaningservices.Adapters.PropertyRecyclerAdapter
import arif.www.wikipedia.com.jmlcleaningservices.Models.Gallery
import arif.www.wikipedia.com.jmlcleaningservices.Models.Property
import arif.www.wikipedia.com.jmlcleaningservices.NetworkModels.GalleryProvider
import arif.www.wikipedia.com.jmlcleaningservices.Utils.SP
import com.github.kittinunf.fuel.httpGet
import kotlinx.android.synthetic.main.activity_properties.*

import kotlinx.android.synthetic.main.activity_property_gallery.*
import kotlinx.android.synthetic.main.content_gallery_request.*

class PropertyGalleryActivity : AppCompatActivity() {

    private lateinit var clientName:String
    private lateinit var location:String
    private var propertyId:Int = 0

    private lateinit var linearLayoutManager: LinearLayoutManager
    var galleryList = ArrayList<Gallery>();
    private lateinit var adapter: GalleryRecyclerAdapter;
    private lateinit var progressBar: ProgressBar;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_gallery)
        setSupportActionBar(toolbar)

        clientName = SP.getString(this, SP.CURRENT_PROPERTY_CLIENT_NAME, "");
        location = SP.getString(this, SP.CURRENT_PROPERTY_LOCATION, "");
        propertyId = SP.getInt(this, SP.CURRENT_PROPERTY_ID, 0);

        progressBar = findViewById(R.id.progressBar);

        fab.setOnClickListener { view ->
            val intent = Intent(this, GalleryRequestJavaActivity::class.java)
            startActivity(intent)
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        galleriesRV.layoutManager = linearLayoutManager

        /*showProgressbar(false)*/
        "/properties/${propertyId}/galleries?token=${SP.getString(this, SP.TOKEN, "")}"
                .httpGet().responseObject(GalleryProvider.Deserializer()) { _, response, result ->
                    val (galleryProvider, error) = result;
                    //*println(PropertyProvider.toString());
                    /*println(response.statusCode);
                    println(response.url);
                    println(error);*/
                    progressBar.visibility = View.GONE;
                    galleryList = galleryProvider?.galleries as ArrayList<Gallery>;
                    if(galleryList.size == 0){
                        noRecordFound.visibility = View.VISIBLE;
                    }else{
                        adapter = GalleryRecyclerAdapter(this, galleryList);
                        galleriesRV.adapter = adapter;
                    }
                }
    }

}
