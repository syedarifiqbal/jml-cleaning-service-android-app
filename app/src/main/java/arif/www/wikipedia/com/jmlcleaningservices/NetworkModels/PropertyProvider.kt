package arif.www.wikipedia.com.jmlcleaningservices.NetworkModels

import arif.www.wikipedia.com.jmlcleaningservices.Models.Property
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class PropertyProvider(
        val current_page: Int,
        val data: List<Property>,
        val first_page_url: String,
        val from: String,
        val last_page: String,
        val last_page_url: String,
        val next_page_url: String,
        val path: String,
        val per_page: String,
        val prev_page_url: String,
        val to: String,
        val total: String
) {

    class Deserializer : ResponseDeserializable<PropertyProvider>{
        override fun deserialize(content: String): PropertyProvider? = Gson().fromJson(content, PropertyProvider::class.java);
    }

}
